package wpl.live.api.proxy;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
public class PublishController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(value = "/on_publish",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String create(@RequestParam("app") String app,
                         @RequestParam("name") String name,
                         @RequestParam("call") String call,
                         @RequestParam("key") Optional<String> key) {
        logger.info("create app={}, name={}, call={}, key={}", app, name, call, key);
        String auth_key = key.orElse(authMap.get(name));
        if (auth_key == null) auth_key = "";
        String title = titleMap.get(name);
        if (title == null) title = DEFAULT_TITLE;

        String url = String.format(CREATE_API, auth_key, URLEncoder.encode(title));
        sendApiRequest(url);
        return "OK";
    }

    @RequestMapping(value = "/on_auth_publish",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String createWithKey(@RequestParam("app") String app,
                                @RequestParam("name") String name,
                                @RequestParam("call") String call,
                                @RequestParam("key") Optional<String> key) {
        logger.info("create app={}, name={}, call={}, key={}", app, name, call, key);
        String auth_key = key.orElse(authMap.get(name));
        if (auth_key == null) throw new IllegalArgumentException("invalid authen key");
        String title = titleMap.get(name);
        if (title == null) title = DEFAULT_TITLE;

        String url = String.format(CREATE_API, auth_key, title);
        sendApiRequest(url);
        return "OK";
    }

    @RequestMapping(value = "/on_play",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String play(@RequestParam("app") String app,
                       @RequestParam("name") String name,
                       @RequestParam("call") String call) {
        logger.info("play app={}, name={}, call={}", app, name, call);
        throw new IllegalArgumentException("invalid action");
    }

    @RequestMapping(value = "/on_done",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String delete(@RequestParam("app") String app,
                         @RequestParam("name") String name,
                         @RequestParam("call") String call,
                         @RequestParam("key") Optional<String> key) {
        logger.info("delete app={}, name={}, call={}, key={}", app, name, call, key);
        String auth_key = key.orElse(authMap.get(name));
        if (auth_key != null) {
            String url = String.format(DELETE_API, auth_key, name);
            sendApiRequest(url);
        }
        return "OK";
    }

    @ExceptionHandler(IllegalArgumentException.class)
    void handleBadAccess(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.SC_UNAUTHORIZED);
    }

    private static final String DEFAULT_TITLE = "Hi";
    private static final String CREATE_API = "http://api.360live.vn/api_stream/create?auth_key=%s&title=%s&type=1&longtitude=0.233&latitude=100.1&city=TP.HCM&country=VietNam";
    private static final String DELETE_API = "http://api.360live.vn/api_stream/delete?auth_key=%s&stream_id=%s";
    private static final Map<String, String> authMap = new HashMap<String, String>() {{
        put("13", "4f99586a53d5e8fa0dfd2de8c443769d95d565ef66e7dff9"); // khuong
        put("107", "e2f605292de31466f8a28cfe1c00163695d565ef66e7dff9");
        put("109", "7b52609a11673074ffb03095ef7fe33e95d565ef66e7dff9"); // long cu
        put("123", "ccdd1ac2e7310e24b62f94c9da40474595d565ef66e7dff9"); // dang
        put("136", "9d23814123ff8cdbd8ebfa362fc06d1195d565ef66e7dff9"); // kim binh bong
        put("175", "5ffe9295090b2f20e1a5df9e3c8c761c95d565ef66e7dff9"); // team 3Q
        put("229", "f6756a4bfdf4c01738b8a6077b69d03c95d565ef66e7dff9"); // csgo
        put("352", "3dd79fc53be45efb4b60f6e2a18a62c395d565ef66e7dff9"); // team gunny
        put("360", "83af390a8897d55c18cd44a1cef8d53195d565ef66e7dff9"); // 360
        put("3939", "effab72f09515bc35158e78d1a9fc94e95d565ef66e7dff9");
        put("4444", "5917e9fd2be8d52563becd260c99db3095d565ef66e7dff9");
        put("5678", "ba1a3e2dff1c3f96b0dad482478b0e5a95d565ef66e7dff9");
        put("6666", "e82720ca59f50e7679af721f42ce62ff95d565ef66e7dff9"); // cu hanh
        put("6868", "bc89913cf5acfa791ed8cd4s1d39c603d95d565ef66e7dff9"); // gunny
        put("7777", "d2ca251bc6035d500a9ead661de3ec9895d565ef66e7dff9"); // virus
        put("8888", "3c3fce1083cc7de1dd67509076df313b95d565ef66e7dff9"); // ngoi sao thoi trang
        put("9999", "8f7deb7b090abdea02e57fff6301c99895d565ef66e7dff9"); // ngoi sao hoang cung
        put("38651", "6287c2f04b50f95788c77d6778f0b03d95d565ef66e7dff9"); // xiao
        put("36528", "32178fecf482c2af993a32751b75a06395d565ef66e7dff9"); // khuong
        put("131313", "bd15c3dcb0238db38b8292ab16b68a4595d565ef66e7dff9");
        put("696969", "2f0c62478a6e2e1b89612c2da8179c1995d565ef66e7dff9"); // dota truyen ky
        put("797979", "e8d18b9c2f9ece853a5f11664e0a6ac295d565ef66e7dff9");
        put("969696", "510b1e80fc5c0390fa30d52720cf0e4395d565ef66e7dff9"); //
    }};

    private static final Map<String, String> titleMap = new HashMap<String, String>() {{
        put("136", "Hello");
        put("6666", "Giải đấu King of 3Q 360mobi");
        put("696969", "DTTK 360mobi Giải Đấu Vương Giả Tranh Bá");
        put("6868", "Chung Kết Đại Chiến Guild 2017");
        put("107", "Kênh Giải Trí");
        put("4444", "Kênh Phim - Hài - Clip");
        put("797979", "Hài Bựa - Game");
        put("131313", "Giải Trí - Thể Thao");
        put("5678", "Quà Tặng Yêu Thương");
        put("3939", "Giải trí cùng Ngũ ACa");
    }};

    private void sendApiRequest(String url) {
        HttpClient client = new HttpClient();
        HttpMethod method = new GetMethod(url);
        try {
            if (client.executeMethod(method) != HttpStatus.SC_OK) {
                logger.error("sendApiRequest {} failed: {}", url, method.getStatusLine());
            }
            byte[] responseBody = method.getResponseBody();
            logger.info("sendApiRequest {} response: {}", url, new String(responseBody));
        } catch (Exception e) {
            logger.error("sendApiRequest {} error: {}", url, e.getMessage());
        } finally {
            method.releaseConnection();
        }
    }
}
