package wpl.live.api.proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

import zingme.photostorage.thrift.ImageType;
import zingme.photostorage.thrift.PhotoItem;
import zingme.photostorage.thrift.PhotoStorageServiceUPool;

@RestController
public class ScreenshotController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static JobQueue queue;

    @Value("${screenshot.cmd}")
    private String CMD_FORMAT;

    @Value("${screenshot.output_folder}")
    private String OUTPUT_FOLDER;

    private String PHOTO_BACKEND = "10.30.71.10:10406";

    @RequestMapping(value = "/screenshot/capture", method = RequestMethod.GET)
    public String screenshot(final @RequestParam("channel") String channel) {
        getQueue().put(() -> {
            long start = System.currentTimeMillis();
            String filename = OUTPUT_FOLDER + "/" + channel + "_" + start + ".jpg";
            File image = new File(filename);
            if (image.exists()) {
                image.delete();
            }

            String cmd = String.format(CMD_FORMAT, channel, filename);
            Process p = Runtime.getRuntime().exec(cmd);
            if (!p.waitFor(10, TimeUnit.SECONDS)) {
                p.destroyForcibly();
                logger.info("capture timeout, channel={}, time={} ms",
                        channel, System.currentTimeMillis() - start);
                return;
            }
            if (image.exists()) {
                try {
                    ByteBuffer bb = ByteBuffer.wrap(Files.readAllBytes(image.toPath()));
                    PhotoItem photoItem = new PhotoItem(bb, ImageType.JPG);
                    PhotoStorageServiceUPool.getInstance("backend", PHOTO_BACKEND, PHOTO_BACKEND)
                            .ow_put(channel, photoItem);
                    logger.info("capture success, channel={}, time={} ms",
                            channel, System.currentTimeMillis() - start);
                } finally {
                    image.delete();
                }
            } else {
                logger.info("capture error, channel={}, time={} ms",
                        channel, System.currentTimeMillis() - start);
            }
        });
        return "OK";
    }

    static synchronized JobQueue getQueue() {
        if (queue == null) queue = new JobQueue(10, 1000);
        return queue;
    }

    static class JobQueue {

        private final ArrayBlockingQueue<QueueCommand> queue;
        private final QueueWorker[] workers;
        private final int workerNum;
        private final int maxLength;

        JobQueue(int workerNum, int maxLength) {
            this.workerNum = workerNum;
            this.maxLength = maxLength;
            this.queue = new ArrayBlockingQueue<>(this.maxLength);
            this.workers = new QueueWorker[workerNum];
            this.start();
            Runtime.getRuntime().addShutdownHook(new Thread(() -> stop()));
        }

        boolean put(QueueCommand i) {
            if (this.queue.remainingCapacity() < this.workerNum) {
                return false;
            }
            boolean ret = false;
            try {
                this.queue.put(i);
                ret = true;
            } catch (InterruptedException ignore) {
            }
            return ret;
        }

        QueueCommand take() {
            QueueCommand cmd = null;
            try {
                cmd = this.queue.take();
            } catch (InterruptedException ignore) {
            }
            return cmd;
        }

        void start() {
            for (int i = 0; i < this.workerNum; i++)
                (workers[i] = new QueueWorker()).start();
        }

        void stop() {
            for (int i = 0; i < this.workerNum; i++)
                workers[i].kill();
        }
    }

    static class QueueWorker extends Thread {

        private final long _msleep_idle = 1000L;
        private boolean isRunning = false;

        QueueWorker() {
            this.isRunning = true;
        }

        void kill() {
            this.isRunning = false;
            this.interrupt();
        }

        @Override
        public void run() {
            while (isRunning) {
                try {
                    QueueCommand command = getQueue().take();
                    if (command != null) {
                        command.execute();
                    } else if (this._msleep_idle > 0L) {
                        Thread.sleep(this._msleep_idle);
                    }
                } catch (Exception ignore) {
                }
            }
        }
    }

    interface QueueCommand {

        void execute() throws Exception;
    }
}
